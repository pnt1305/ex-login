import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

import { Observable, of } from 'rxjs';
import { tap, delay } from 'rxjs/operators';

@Injectable({
  providedIn: 'root',
})
export class DashboardService {
  constructor(private http: HttpClient) {}

  callGetStudentsRecords(): Observable<any> {
    return this.http.get('/api/list-student');
  }

  callGetStudentsDetailsRecords(): Observable<any> {
    return this.http.get('/api/studentsDetails');
  }

  callGetStudentsAddressRecords(): Observable<any> {
    return this.http.get('/api/addresses');
  }
}

// // Get call method
// // Param 1 : url
// get(url: string): Observable<any> {
//   const httpOptions = {
//     headers: new HttpHeaders({
//       'Content-Type':  'application/json',
//       'Cache-Control' : 'no-cache',
//       'Pragma' : 'no-cache'
//     }),
//     observe: "response" as 'body'
//   };
//   return this.httpClient.get(
//     url,
//     httpOptions
//   )
//   .pipe(
//       map((response: any) => this.ReturnResponseData(response)),
//       catchError(this.handleError)
//   );
// }

// // Post call method
// // Param 1 : url
// // Param 2 : model
// post(url: string, model: any): Observable<any> {
//   const httpOptions = {
//     headers: new HttpHeaders({
//       'Content-Type': 'application/json'
//     }),
//     observe: "response" as 'body'
//   };
//   return this.httpClient.post(
//     url,
//     model,
//     httpOptions)
//     .pipe(
//       map((response: any) => this.ReturnResponseData(response)),
//       catchError(this.handleError)

//   );
// }
