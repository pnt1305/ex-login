import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';

import { AgGridModule } from 'ag-grid-angular';
import { LandingComponent } from './landing/landing.component';
import {
  NgbDatepickerModule,
  NgbHighlight,
  NgbTypeaheadModule,
} from '@ng-bootstrap/ng-bootstrap';
import { DashboardComponent } from './dashboard/dashboard.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';
import { StudentRecordsComponent } from './student-records/student-records.component';
import { SideBarComponent } from './side-bar/side-bar.component';
import { DashboardService } from './dashboard.service';
import { HttpClientModule } from '@angular/common/http';

const routes: Routes = [
  {
    path: '',
    component: DashboardComponent,
    children: [
      {
        path: '',
        redirectTo: '/dashboard/landing',
        pathMatch: 'full',
      },
      {
        path: 'landing',
        component: LandingComponent,
      },
      {
        path: 'students',
        component: StudentRecordsComponent,
      },
    ],
  },
];

@NgModule({
  declarations: [
    DashboardComponent,
    LandingComponent,
    SideBarComponent,
    StudentRecordsComponent,
  ],
  imports: [
    CommonModule,
    RouterModule,
    HttpClientModule,
    NgbHighlight,
    NgbDatepickerModule,
    NgbTypeaheadModule,
    AgGridModule,
    FormsModule,
    ReactiveFormsModule,
    RouterModule.forChild(routes),
  ],
  providers: [DashboardService],
})
export class DashboardModule {}
